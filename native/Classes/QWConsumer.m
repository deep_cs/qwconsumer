#import "QWConsumer.h"
#import "QWPlacesContainer.h"
#import "QWWebViewController.h"

@implementation QWConsumer

- (void)showPlaces:(CDVInvokedUrlCommand*)command
{
    QWPlacesContainer *placesContainer = [[QWPlacesContainer alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:[[command arguments] objectAtIndex:0]];
    placesContainer.data = data;
   
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:placesContainer];
    [self.viewController presentViewController:navigationController animated:YES completion:nil];
}

- (void) loadQWWebView: (CDVInvokedUrlCommand*) command{
    QWWebViewController *webView = [[QWWebViewController alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:[[command arguments] objectAtIndex:0]];
    webView.data = data;
    [self.viewController presentViewController:webView animated:YES completion:nil];
}

@end
