#import <Cordova/CDV.h>

@interface QWConsumer : CDVPlugin

- (void) showPlaces:(CDVInvokedUrlCommand*)command;

- (void) loadQWWebView: (CDVInvokedUrlCommand*) command;

@end
