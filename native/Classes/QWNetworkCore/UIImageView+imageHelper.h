//
//  UIImageView+imageHelper.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageView(imageHelper) <NSURLSessionDelegate, NSURLSessionDataDelegate>
- (void) qwSetImageWithURL :(NSString *)imageUrl : (NSString *) placeholderImage;
@end
