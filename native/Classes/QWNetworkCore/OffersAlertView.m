//
//  OffersAlertView.m
//  QuikWallet
//
//  Created by Monideep Purkayastha on 02/02/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import "OffersAlertView.h"
#import "alertViewOfferCell.h"
#import "OfferDetailsViewController.h"

@interface OffersAlertView ()

@end

@implementation OffersAlertView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.offersTable.delegate = self;
    self.offersTable.dataSource = self;


    self.offersTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.offers count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"myCell";
    
    NSDictionary *offer = [self.offers objectAtIndex:indexPath.row];
    
    alertViewOfferCell * cell = [self.offersTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
    if (cell == nil) {
        [self.offersTable registerNib:[UINib nibWithNibName:@"alertViewOfferCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        cell = [self.offersTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
        
    cell.ldesc.text = [offer objectForKey:@"ldesc"];
    cell.ldesc.adjustsFontSizeToFitWidth = NO;
    cell.ldesc.numberOfLines = 0;
    cell.ldesc.lineBreakMode = NSLineBreakByWordWrapping;
        
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OfferDetailsViewController *offerDetails = [[OfferDetailsViewController alloc] init];
    offerDetails.offer = [self.offers objectAtIndex:indexPath.row];
    offerDetails.placeDetailsFlag = true;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigation pushViewController:offerDetails animated:YES];
}




@end
