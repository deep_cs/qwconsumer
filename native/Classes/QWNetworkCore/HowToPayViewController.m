//
//  HowToPayViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 20/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import "HowToPayViewController.h"
#import "QWHowToPayCell.h"

@interface HowToPayViewController ()

@end

@implementation HowToPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    self.howToPayTable.delegate = self;
    self.howToPayTable.dataSource = self;
    
    // This will remove extra separators from tableview
    self.howToPayTable.tableFooterView = [UIView new];
    
    self.paymentTypes = [[NSMutableArray alloc] init];
    
    if ([self.howToPayArray count] != 0) {
        
        NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
        [payload setObject:@"Redemptions" forKey:@"redemption"];
        [payload setObject:@"Amount" forKey:@"amount"];
        [self.paymentTypes addObject:payload];
    }
    
    for(int i=0;i<[self.howToPayArray count];i++)
    {
        NSMutableDictionary *method = self.howToPayArray[i];
        
        NSString *methodname = [method objectForKey:@"method"];
        
        NSArray *payloadarray = [method objectForKey:@"payload"];
        
        if([methodname isEqualToString:@"prepaid"])
        {
            for(int j=0;j<[payloadarray count];j++)
            {
                NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
                if([payloadarray[j] objectForKey:@"metacardname"]!=nil)
                {
                    [payload setObject:[payloadarray[j] objectForKey:@"metacardname"] forKey:@"name"];
                }
                else
                {
                    [payload setObject:@"Prepaid" forKey:@"name"];
                }
                [payload setObject:[payloadarray[j] objectForKey:@"amount"] forKey:@"amount"];
                [self.paymentTypes addObject:payload];
            }
        }
        else if([methodname isEqualToString:@"offer"])
        {
            for(int j=0;j<[payloadarray count];j++)
            {
                NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
                [payload setObject:[payloadarray[j] objectForKey:@"sdesc"] forKey:@"name"];
                [payload setObject:[payloadarray[j] objectForKey:@"amount"] forKey:@"amount"];
                [self.paymentTypes addObject:payload];
            }
        }
        else if([methodname isEqualToString:@"credit"])
        {
            for(int j=0;j<[payloadarray count];j++)
            {
                NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
                [payload setObject:@"QWCredit" forKey:@"name"];
                [payload setObject:[payloadarray[j] objectForKey:@"amount"] forKey:@"amount"];
                [self.paymentTypes addObject:payload];
            }
        } else if ([methodname isEqualToString:@"partnerwallet"]){
            for(int j=0;j<[payloadarray count];j++)
            {
                NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
                [payload setObject:[payloadarray[j] objectForKey:@"name"] forKey:@"name"];
                [payload setObject:[payloadarray[j] objectForKey:@"amount"] forKey:@"amount"];
                [self.paymentTypes addObject:payload];
            }

        }
        
    }
    
    if ([self.howToPayArray count] != 0) {
        NSMutableDictionary *payload = [[NSMutableDictionary alloc]init];
        [payload setObject:@"You Pay" forKey:@"name"];
        [payload setObject:self.toPay forKey:@"amount"];
        [self.paymentTypes addObject:payload];
    }
    
    NSLog(@"PAYMENT TYPES %@", self.paymentTypes);
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.paymentTypes count];
}

- (void)dealloc
{
    self.howToPayTable.dataSource = nil;
    self.howToPayTable.delegate = nil;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *MyIdentifier = @"howToPayCell";
    
    QWHowToPayCell * cell = [self.howToPayTable dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        [self.howToPayTable registerNib:[UINib nibWithNibName:@"QWHowToPayCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell = [self.howToPayTable dequeueReusableCellWithIdentifier:MyIdentifier];
    }
    
    NSDictionary *paymenttype = [self.paymentTypes objectAtIndex:indexPath.row];
    
    if(indexPath.row == 0)
    {
        cell.redemptionLbl.text = [paymenttype objectForKey:@"redemption"];
        cell.amountLbl.text = [paymenttype objectForKey:@"amount"];
        [cell.redemptionLbl setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
        [cell.amountLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    }else
    {
        NSString *rupeeSymbol= @"₹";
        cell.redemptionLbl.text = [paymenttype objectForKey:@"name"];
        NSString *amount = [paymenttype objectForKey:@"amount"];
        NSString *amountstring = [NSString stringWithFormat:@"%@",amount];
        cell.amountLbl.text = [rupeeSymbol stringByAppendingString:amountstring];
    }
    
    if(indexPath.row == [paymenttype count])
    {
        [cell.redemptionLbl setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
        [cell.amountLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    }
    
    return cell;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}*/

@end
