//
//  QuikPayViewController.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 08/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWQuikPayViewController.h"
#import "NSString+FormValidation.h"
#import "QWActivityHelper.h"
#import "QWPayNowViewController.h"
#import "QWPayNowViewController.h"
#import "QWConsumerConstants.h"
#import "QWQRPayViewController.h"
#import "QWPlaceDetailsViewController.h"
#import "QWSdk.h"

@interface QWQuikPayViewController ()

@end

@implementation QWQuikPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Close Button
    UIImage* image4 = [UIImage imageNamed:@"icon_close.png"];
    CGRect frameimg1 = CGRectMake(0, 0, 15, 15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg1];
    backButton.contentMode = UIViewContentModeScaleAspectFit;
    [backButton setBackgroundImage:image4 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    self.navigationItem.title = @"QUIKPAY";
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.floatingAmount.inputAccessoryView = keyboardDoneButtonView;
    self.floatingQuikID.inputAccessoryView = keyboardDoneButtonView;

    self.isReading = NO;
    self.captureSession = nil;
    self.isCaptured = NO;
}

- (NSString *)validateForm {
    NSString *errorMessage;
    UITextField *viewWithError;
    
    if (IsEmpty(self.floatingQuikID.text)){
        errorMessage = @"Please enter a valid Quik ID";
        viewWithError = self.floatingQuikID;
        
    }
    else if (![self.floatingAmount.text  isValidAmount]){
        errorMessage = @"Please enter a valid amount";
        viewWithError = self.floatingAmount;
    }
    
    return errorMessage;
}

- (IBAction)quikPay:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateForm];
    
    if(errorMessage)
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        if ([UIAlertController class]) {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:errorMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message: errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        QWPayNowViewController *pay = [[QWPayNowViewController alloc] init];
        
        [self.data setObject:self.floatingAmount.text forKey:@"amount"];
        [self.data setObject:self.floatingQuikID.text forKey:@"outletid"];
        
        pay.data = self.data;
        [pay.data setObject:SDK_ACTION forKey:INTENDED_ACTION_FROM_SDK];
        pay.presentationStyle = @"presentViewController";
        [self.navigationController pushViewController:pay animated:YES];
    }
}

#pragma mark QRPay Implementation
- (IBAction)startReadingQRCode:(id)sender {
    if (!self.isReading) {
        
        if ([self startReading]) {
            self.startBtn.hidden = YES;
        }
        
    }else{
        [self stopReading];
        self.startBtn.hidden = NO;
    }
    
    self.isReading = !self.isReading;
    _isCaptured = NO;
}

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}

-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    _isReading = NO;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [self.videoPreviewLayer removeFromSuperlayer];
    self.startBtn.hidden= NO;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{

    if (!_isCaptured) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [QWActivityHelper displayActivityIndicator:self.view];
        });
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if (metadataObjects != nil && [metadataObjects count] > 0) {
            // Get the metadata object.
            AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
            if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
                NSLog(@"QR CODE %@", [metadataObj stringValue]);
                _isCaptured = YES;
                
                [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
                
                NSDictionary *dict = @{
                                       @"terminalkey":[metadataObj stringValue],
                                       @"partnerid" : [self.data objectForKey:@"partnerid"],
                                       @"mobile" : [self.data objectForKey:@"mobile"],
                                       @"signature" : [self.data objectForKey:@"signature"],
                                       @"transactionId": [self.data objectForKey:@"transactionId"]
                                       };
                
                NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
                
                void(^success)() = ^void(id  data){
                    
                    if ([data objectForKey:@"payment"] && !IsEmpty([data objectForKey:@"payment"])) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self createPayment:data];
                            [QWActivityHelper removeActivityIndicator:self.view];
                            [self stopReading];
                        });
                    } else if([data objectForKey:@"place"] && !IsEmpty([data objectForKey:@"place"])){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            QWPlaceDetailsViewController *placeDetails = [[QWPlaceDetailsViewController alloc] init];
                            placeDetails.currentPlace = [data objectForKey:@"place"];
                            placeDetails.data = self.data;
                            placeDetails.presentationStyle = @"presentViewController";
                            [self.navigationController pushViewController:placeDetails animated:YES];
                            
                            [QWActivityHelper removeActivityIndicator:self.view];
                            [self stopReading];
                        });
                    }
                };
                
                void(^failure)() = ^void(NSError* error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [QWActivityHelper removeActivityIndicator:self.view];
                        [self stopReading];
                    });
                    
                    NSDictionary *errorInfo = [error userInfo];
                    
                    if ([UIAlertController class]){
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                                       message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {}];
                        
                        [alert addAction:defaultAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    } else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                    
                };
                
                
                QWSdk *qw = [[QWSdk alloc] init];
                [qw requestBill:data :success :failure];
            }
        }
        
    }
}

-(void)createPayment:(NSDictionary *)responseObject
{
    QWQRPayViewController *qr = [[QWQRPayViewController alloc] init];
    qr.paymentObject = [responseObject objectForKey:@"payment"];
    qr.howtopayData = [responseObject objectForKey:@"howtopay"];
    qr.data = self.data;
    qr.presentationStyle = @"presentViewController";
    [self.navigationController pushViewController:qr animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) popBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end
