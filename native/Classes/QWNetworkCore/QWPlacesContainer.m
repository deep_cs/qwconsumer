//
//  QWPlacesContainer.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWPlacesContainer.h"
#import "HMSegmentedControl.h"
#import "QWActivityHelper.h"
#import "QWPlacesViewController.h"
#import "GlobalSingleton.h"
#import <CoreLocation/CoreLocation.h>
#import "QWSdk.h"

@interface QWPlacesContainer ()

@end

NSArray *_pages;
HMSegmentedControl *segmentedControl;

@implementation QWPlacesContainer

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        
    }
    return self;
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveFeedbackNotification:)
                                                 name:@"FeedbackNotification"
                                               object:nil];
    
    // self.navigationItem.title = @"PLACES NEAR YOU ..";
    
    UIImage *image = [UIImage imageNamed:@"qwhead.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];

    
    // Menu
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"FOOD & DRINK", @"RETAIL"]];
    segmentedControl.frame = CGRectMake(0, 0, viewWidth , 50);
    
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentedControl.selectionIndicatorColor = [UIColor darkGrayColor];
    segmentedControl.selectionIndicatorHeight = 0.0;
    segmentedControl.backgroundColor = [UIColor whiteColor];
    segmentedControl.textColor = [UIColor lightGrayColor];
    segmentedControl.selectedTextColor = [UIColor blackColor];
    segmentedControl.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
    
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    // Add a border
    UIView *border = [[UIView alloc]initWithFrame:CGRectMake(0, 50, viewWidth, 1)];
    [border setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0]];
    border.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:border];
    
    // Close Button
    UIImage* image4 = [UIImage imageNamed:@"icon_close.png"];
    CGRect frameimg1 = CGRectMake(0, 0, 15, 15);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg1];
    backButton.contentMode = UIViewContentModeScaleAspectFit;
    [backButton setBackgroundImage:image4 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    // Get Places
    [self getPlaces];
    
}

- (void) getPlaces{
    
    // Display spinner
    [QWActivityHelper displayActivityIndicator:self.view];
    
    void(^success)() = ^void(id  data){
        self.places = [self sortPlaces:data];
        self.offers = [data objectForKey:@"offers"];
        
        GlobalSingleton *global = [GlobalSingleton sharedInstance];
        global.offers = [NSArray arrayWithArray:[data objectForKey:@"offers"]];
        
        NSMutableArray *retailsTemp = [NSMutableArray array];
        NSMutableArray *fnbsTemp = [NSMutableArray array];
        
        for(NSDictionary *item in self.places) {
            if ([[item objectForKey: @"categoryid"] intValue] == 1) {
                [retailsTemp addObject: item];
            } else if ([[item objectForKey: @"categoryid"] intValue] == 3){
                [fnbsTemp addObject: item];
            }
        }
        
        self.retails = [NSArray arrayWithArray: retailsTemp];
        self.fnbs = [NSArray arrayWithArray: fnbsTemp];
        
        [self setupPageView];
        
        [QWActivityHelper removeActivityIndicator:self.view];
        
    };
    
    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        [self setupPageView];
        
        NSDictionary *errorInfo = [error userInfo];
        
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw getPlaces:self.data :success :failure];
}

- (void) popBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableArray *)sortPlaces : (NSMutableDictionary *) responseData{
    // Sort and Update places
    NSMutableArray *newPlaceData = [NSMutableArray arrayWithArray: [responseData objectForKey:@"places"]];
    NSMutableArray *sortedPlaces = [[NSMutableArray alloc]init];
    
    double lat = [[self.data objectForKey:@"latitude"] doubleValue];
    double longi = [[self.data objectForKey:@"longitude"] doubleValue];
    
    CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:lat longitude:longi];
    
    for(NSMutableDictionary *place in newPlaceData){
        CLLocation *placeLocation = [[CLLocation alloc]initWithLatitude: [[place objectForKey:@"latitude"] doubleValue]
                                                              longitude:[[place objectForKey:@"longitude"] doubleValue]];
        
        CLLocationDistance distanceInMeters = [currentLocation distanceFromLocation:placeLocation]/1000;
        
        NSMutableDictionary *distances = [[NSMutableDictionary alloc] initWithDictionary:place];
        
        [distances setObject:[NSNumber numberWithFloat:distanceInMeters] forKey:@"distance"];
        
        [sortedPlaces addObject:distances];
    }
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                 ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [sortedPlaces sortedArrayUsingDescriptors:sortDescriptors];
    newPlaceData = [NSMutableArray arrayWithArray:sortedArray];
    
    return newPlaceData;
}

- (void) setupPageView{
    [self setupPages];
    
    self.pageView = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageView.dataSource = self;
    self.pageView.delegate = self;
    
    [[self.pageView view] setFrame:CGRectMake(0, 51, [[self view] bounds].size.width, [[self view] bounds].size.height + 37 - 51)];
    self.pageView.view.tag = 207;
    
    NSArray *viewControllers = [NSArray arrayWithObject:_pages[0]];
    [self.pageView setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageView];
    [[self view] addSubview:[self.pageView view]];
    [self.pageView didMoveToParentViewController:self];
}

- (void) setupPages {
    
    QWPlacesViewController *places = [[QWPlacesViewController alloc] init];
    places.screenIndex = 0;
    places.places = self.fnbs;
    places.data = self.data;
    
    QWPlacesViewController *retails = [[QWPlacesViewController alloc] init];
    retails.screenIndex = 1;
    retails.places = self.retails;
    retails.data = self.data;
    
    _pages = [NSArray arrayWithObjects: places, retails, nil];
}

#pragma mark    UIPageViewController implementaion
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if (nil == viewController) {
        return _pages[0];
    }
    NSInteger idx = [_pages indexOfObject:viewController];
    NSParameterAssert(idx != NSNotFound);
    if (idx >= [_pages count] - 1) {
        // we're at the end of the _pages array
        return nil;
    }
    // return the next page's view controller
    return _pages[idx + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if (nil == viewController) {
        return _pages[0];
    }
    NSInteger idx = [_pages indexOfObject:viewController];
    NSParameterAssert(idx != NSNotFound);
    if (idx <= 0) {
        // we're at the end of the _pages array
        return nil;
    }
    // return the previous page's view controller
    return _pages[idx - 1];
}

- (NSInteger) presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return  [_pages count];
}

- (NSInteger) presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (void) pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    UIViewController * currentView = [pageViewController.viewControllers objectAtIndex:0];
    
    NSUInteger index = [(QWPlacesViewController *) currentView screenIndex];
    [segmentedControl setSelectedSegmentIndex: index animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    UIViewController *initial =   _pages[segmentedControl.selectedSegmentIndex];
    NSArray *viewControllers = [NSArray arrayWithObject:initial];
    
    [self.pageView setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];

}

- (void) receiveFeedbackNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"FeedbackNotification"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Thanks for your feedback."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    
}

@end
