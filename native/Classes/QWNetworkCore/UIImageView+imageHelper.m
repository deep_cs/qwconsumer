//
//  UIImageView+imageHelper.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "UIImageView+imageHelper.h"

@implementation UIImageView (imageHelper)

- (void) qwSetImageWithURL: (NSString *)imageUrl :(NSString *)placeholderImage{
    
    self.image = [UIImage imageNamed:placeholderImage];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [configuration setHTTPAdditionalHeaders: @{@"Accept": @"application/json"}];
    
    configuration.timeoutIntervalForRequest = 30.0;
    configuration.timeoutIntervalForResource = 60.0;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:imageUrl];
    
    NSURLSessionDownloadTask *getImageTask = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response,NSError *error) {
        
        if (error) {
            NSLog(@"FAILURE %@",error);
        }else{
            if ([(NSHTTPURLResponse *) response statusCode] == 200) {
                UIImage *downloadedImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:location]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = downloadedImage;
                });
            } else{
                NSLog(@"Couldn't load image at URL: %@",url);
                NSLog(@"HTTP %ld", (long) [(NSHTTPURLResponse *)response statusCode]);
            }
        }
    }];
    
    [getImageTask resume];
}

@end
