//
//  QWPlaceCell.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWPlaceCell.h"

@implementation QWPlaceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
