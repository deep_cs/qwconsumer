//
//  OfferDetailsViewController.h
//  QuikWallet
//
//  Created by Monideep Purkayastha on 02/02/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *ldesc;
@property (weak, nonatomic) IBOutlet UILabel *sdesc;
@property (weak, nonatomic) IBOutlet UILabel *terms;
@property (weak, nonatomic) IBOutlet UILabel *termsText;
@property (nonatomic, copy) NSMutableDictionary *offer;
@property (nonatomic, assign) Boolean placeDetailsFlag;
@property (weak, nonatomic) IBOutlet UILabel *statictnc;
@property (weak, nonatomic) IBOutlet UIButton *showPlaceBtn;
@end
