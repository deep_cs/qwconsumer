//
//  QWPayNowViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 20/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HowToPayViewController.h"

@interface QWPayNowViewController : UIViewController
@property(nonatomic, strong) NSMutableDictionary *data;
@property (weak, nonatomic) IBOutlet UIView *howToPayView;
@property (weak, nonatomic) IBOutlet UIView *grandTotalView;
@property (nonatomic, strong) NSMutableDictionary *paymentData;
- (IBAction)proceedToPayment:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalLbl;
@property (nonatomic, strong) HowToPayViewController *howToPay;
@property (nonatomic, copy) NSString * presentationStyle;

@end
