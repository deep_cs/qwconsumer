//
//  ReportIssueViewController.m
//  QWConsumer
//
//  Created by Monideep Purkayastha on 18/04/16.
//
//

#import "QWReportIssueViewController.h"

@interface QWReportIssueViewController ()

@end

@implementation QWReportIssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.message.inputAccessoryView = keyboardDoneButtonView;
    self.message.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [self.message becomeFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

@end
