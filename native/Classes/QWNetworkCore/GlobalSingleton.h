//
//  GlobalSingleton.h
//  QWConsumer
//
//  Created by Monideep Purkayastha on 19/04/16.
//
//

#import <Foundation/Foundation.h>

@interface GlobalSingleton : NSObject{
    NSArray * _offers;
}

@property (nonatomic, retain, readwrite) NSArray *offers;

+ (GlobalSingleton *)sharedInstance;

@end
