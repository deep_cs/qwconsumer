//
//  ReportIssueViewController.h
//  QWConsumer
//
//  Created by Monideep Purkayastha on 18/04/16.
//
//

#import <UIKit/UIKit.h>

@interface QWReportIssueViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *message;
@end
