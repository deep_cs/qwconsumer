//
//  QWPlacesViewController.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWPlacesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (assign, nonatomic) NSInteger screenIndex;
@property (nonatomic, copy) NSArray *places;
@property (nonatomic, copy) NSArray *offers;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *data;
@property (nonatomic, strong) NSString *outLetId;
- (IBAction)quikPay:(id)sender;
@end
