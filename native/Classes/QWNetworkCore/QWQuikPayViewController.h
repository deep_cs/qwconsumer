//
//  QuikPayViewController.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 08/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface QWQuikPayViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UITextField *floatingQuikID;
@property (weak, nonatomic) IBOutlet UITextField *floatingAmount;
@property (weak, nonatomic) IBOutlet UIButton *quikpaybuton;
@property (strong, nonatomic) NSString *paymenthash;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL isCaptured;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
- (IBAction)quikPay:(id)sender;
- (IBAction)startReadingQRCode:(id)sender;
@property (nonatomic, strong) NSMutableDictionary *data;
@end
