//
//  GlobalSingleton.m
//  QWConsumer
//
//  Created by Monideep Purkayastha on 19/04/16.
//
//

#import "GlobalSingleton.h"

@implementation GlobalSingleton

@synthesize offers = _offers;

+ (GlobalSingleton *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalSingleton *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalSingleton alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}


@end
