
/* JavaScript content from js/main.js in folder common */
function wlCommonInit(){
	/*
	 * Use of WL.Client.connect() API before any connectivity to a MobileFirst Server is required. 
	 * This API should be called only once, before any other WL.Client methods that communicate with the MobileFirst Server.
	 * Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	 *    
	 *    WL.Client.connect({
	 *    		onSuccess: onConnectSuccess,
	 *    		onFailure: onConnectFailure
	 *    });
	 *     
	 */
	
	// Common initialization code goes here
	
	var button = document.getElementById("loadSDK");
	
	button.addEventListener('click', function(e){

		var success = function(message) {
	        console.log("Success");
	    }

	    var failure = function() {
	        alert("Error calling QW_NETWORK Plugin");
	    }

	    var data = {
	      "mobile":"8000000000",
	      "signature": "d456a49eca7a42a0e2cbfcaab6898a2d4a4a0e1a00003a3f85f0241d55606ad4",
	      "partnerid": "156",
          "transactionId": "10123", // Please enter correct transaction id
	      "latitude" : "19.10805395867045",
	      "longitude": "73.01890555271596"
	    };

	    qwnetwork.showPlaces(data, success, failure);
	}, false);
	
	
}

/* JavaScript content from js/main.js in folder iphone */
// This method is invoked after loading the main HTML and successful initialization of the IBM MobileFirst Platform runtime.
function wlEnvInit(){
    wlCommonInit();
    // Environment initialization code goes here
}